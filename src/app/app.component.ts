import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
// import { DetailPage } from '../pages/detail/detail';
import { ListUstadz } from '../pages/listUstadz/listUstadz';
import { ListJadwal } from '../pages/listJadwal/listJadwal';
import { RegisPage } from '../pages/regis/regis';
// import { DetailUstadz } from '../pages/detailUstadz/detailUstadz';
import { NativeStorage } from '@ionic-native/native-storage';
import { About } from '../pages/about/about';
import { LogoutPage } from '../pages/logout/logout';
import { Events } from 'ionic-angular';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = RegisPage;
  dataUser: any = [];

  loggedIn = false;

  loggedInPages = [
    { title: 'Logout', component: 'PageTwo' }
  ];

  loggedOutPages = [
    { title: 'Login', component: 'PageOne' }
  ];


  pages: Array<{icon: string, title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private nativeStorage: NativeStorage, public events: Events) {

    this.initializeApp();
    this.listenToLoginEvents();

    this.nativeStorage.getItem('user')
    .then(
      data => {
        console.log(data);
        console.log('login');
        this.dataUser = data;
        this.events.publish('user:login');

      },
      error => {
        console.log('logout');
        console.error(error);
        this.events.publish('user:logout');

      }
    );


  }

  listenToLoginEvents() {
    this.events.subscribe('user:login', () => {

      this.nativeStorage.getItem('user')
      .then(
        data => {
          console.log(data);
          console.log('login from even');
          this.dataUser = data;

        },
        error => {
          console.log('logout');
          console.error(error);

        }
      );

      this.loggedIn = true;
      this.pages = [
        { icon:'home', title: 'Home', component: ListPage },
        { icon:'md-list-box', title: 'List Ustadz', component: ListUstadz },
        // { title: 'List Jadwal', component: ListJadwal },
        { icon:'open', title: 'About us', component: About },
        { icon:'log-out', title: 'Logout', component: LogoutPage }
      ];
      console.log('login');
    });

    this.events.subscribe('user:logout', () => {
      console.log('logout');
      this.pages = [
        { icon:'home', title: 'Home', component: ListPage },
        { icon:'md-list-box', title: 'List Ustadz ', component: ListUstadz },
        // { title: 'List Jadwal', component: ListJadwal },
        { icon:'log-in', title: 'Login | Register', component: RegisPage },
        { icon:'open', title: 'About us', component: About }
      ];
      this.loggedIn = false;
    });
  }


  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
