import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { DetailPage } from '../pages/detail/detail';
import { ListUstadz } from '../pages/listUstadz/listUstadz';
import { ListJadwal } from '../pages/listJadwal/listJadwal';
import { RegisPage } from '../pages/regis/regis';
import { DetailUstadz } from '../pages/detailUstadz/detailUstadz';
import { About } from '../pages/about/about';
import { LogoutPage } from '../pages/logout/logout';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HttpModule } from '@angular/http';
import { NativeStorage } from '@ionic-native/native-storage';
import { OneSignal } from '@ionic-native/onesignal';
import { Authentication } from './auth';
import { GoogleAnalytics } from '@ionic-native/google-analytics';




@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    DetailPage,
    ListUstadz,
    ListJadwal,
    RegisPage,
    DetailUstadz,
    About,
    LogoutPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    DetailPage,
    ListUstadz,
    RegisPage,
    ListJadwal,
    DetailUstadz,
    About,
    LogoutPage
  ],
  providers: [
    StatusBar,
    OneSignal,
    GoogleAnalytics,
    NativeStorage,
    Authentication,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
