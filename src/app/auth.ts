import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Component({
  // selector: 'page-home',
  // templateUrl: 'home.html'
})
export class Authentication {

  activeUser = new BehaviorSubject(null);

  constructor(public navCtrl: NavController) {
    console.log('auth');
  }

  doLogin() {
    this.activeUser.next({ username: 'aaron@mail.com'})
  }

  doLogout() {
     this.activeUser.next(null)
  }

}
