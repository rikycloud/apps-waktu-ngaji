import { Component } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { Http } from '@angular/http';
import { RegisPage } from '../regis/regis';
import { ListPage } from '../list/list';
import { NativeStorage } from '@ionic-native/native-storage';

import 'rxjs/add/operator/map';
import 'rxjs/Rx';


@Component({
  selector: 'page-home',
  templateUrl: 'logout.html'
})
export class LogoutPage {
  menuList: string = "login"
  postData = {}

  constructor(public navCtrl: NavController, private nativeStorage: NativeStorage, public events: Events) {
    this.nativeStorage.remove('user')
    .then(
      data => {
        console.log('logout berhasil');
        this.events.publish('user:logout');
        this.navCtrl.setRoot(RegisPage, {});
      },
      error => {
        console.log('logout gagal');
        this.events.publish('user:login');
        console.error(error)
        this.navCtrl.setRoot(ListPage, {});

      }
    );
  }


}
