import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { DetailUstadz } from '../detailUstadz/detailUstadz';
import { Http } from '@angular/http';


@Component({
  selector: 'page-home',
  templateUrl: 'listUstadz.html'
})
export class ListUstadz {

  ustadz: string[];

  constructor(public navCtrl: NavController, public http: Http, public loadingCtrl: LoadingController) {
    this.ustadz = [];
    let urlUstadz = "http://www.waktungaji.com/apis/v2/";
    this.http.get(urlUstadz).map(res => res.json()).subscribe(data => {
      this.ustadz = data.list_ustadz;
      console.log(data.list_ustadz);
    });
  }

  detailUstadz(detail) {
    this.navCtrl.push(DetailUstadz, {
      detail: detail
    });
  }

}
