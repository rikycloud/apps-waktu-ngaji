import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Component({
  selector: 'page-home',
  templateUrl: 'detail.html'
})
export class DetailPage {

    detail:string[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
      this.detail = navParams.get('detail');
      if(this.detail == null) {
        let url = "http://www.waktungaji.com/apis/v2/jadwal/"+navParams.get('id');
        this.detail = [];
        this.http.get(url).map(res => res.json()).subscribe(data => {
          this.detail = data.jadwal_detail[0];
          console.log('detail');
          console.log(data);
          console.log(data.jadwal_detail[0]);
          console.log('detail row');
          console.log(this.detail);
        });
      }
  }

}
