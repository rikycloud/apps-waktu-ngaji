import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'about.html'
})
export class About {

  constructor(public navCtrl: NavController) {

  }

}
