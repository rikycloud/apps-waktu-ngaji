import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import { ListPage } from '../list/list';
import { NativeStorage } from '@ionic-native/native-storage';
import { Events, ToastController } from 'ionic-angular';

import 'rxjs/add/operator/map';
import 'rxjs/Rx';


@Component({
  selector: 'page-home',
  templateUrl: 'regis.html'
})
export class RegisPage {
  menuList: string = "login"
  postData = {};
  postDataregis = {
    "email":"",
    "password":"",
    "nama_lengkap":"",
    "level":2
  };

  constructor(public navCtrl: NavController, public http: Http, private nativeStorage: NativeStorage, public events: Events, public toastCtrl: ToastController) {

    this.nativeStorage.getItem('user')
    .then(
      data => {
        this.events.publish('user:login');
        this.navCtrl.setRoot(ListPage, {});
      },
      error => {
        this.events.publish('user:logout');

      }
    );

  }

  logForm() {
    // console.log(this.postData)

    this.http.post("http://www.waktungaji.com/apis/v2/user/signin", JSON.stringify(this.postData)).map(res => res.json()).subscribe(data => {

      console.log('respons');
      if(data.error == 0) {
        this.setUserLogin(data.user_data[0]);
        let toast = this.toastCtrl.create({
          message: 'Selamat datang '+data.user_data[0].nama_user,
          duration: 3000,
          position: 'bottom'
        });
        toast.present(toast);



      } else {
        // console.log(data.user_data);
        this.postData = {
          "email":"",
          "password":""
        };
        let toast = this.toastCtrl.create({
          message: 'User tidak ditemukan, silahkan coba lagi.',
          duration: 3000,
          position: 'bottom'
        });
        toast.present(toast);

      }
    });

  }

  setUserLogin(data) {
    this.nativeStorage.setItem('user', data)
    .then(
      () => {
        console.log('Stored item 2!');
        this.events.publish('user:login');
        this.navCtrl.setRoot(ListPage, {});
      },
      error => console.error('Error storing item', error)
    );
  }

  regForm() {
    console.log('regis');
    console.log(this.postDataregis);
    console.log('22');
    console.log(JSON.stringify(this.postDataregis));

    this.http.post("http://www.waktungaji.com/apis/v2/user/signup", JSON.stringify(this.postDataregis)).map(res => res.json()).subscribe(data => {
      // this.setUserLogin(data.user_data[0]);
      console.log('register');
      console.log(data);
      // console.log(data.user_data[0]);
      // console.log(data.error);
      if(data.error == 0) {
      let toast = this.toastCtrl.create({
        message: 'Register berhasil, silahkan login.',
        duration: 3000,
        position: 'bottom'
      });
      toast.present(toast);
      this.postDataregis = {
        "email":"",
        "password":"",
        "nama_lengkap":"",
        "level":2
      };

      this.menuList = "login";
      } else {

        let toast = this.toastCtrl.create({
          message: 'Register gagal.',
          duration: 3000,
          position: 'bottom'
        });
        toast.present(toast);
        console.log('gagal');
        console.log(data.user_data);

      }
    });
  }

}
