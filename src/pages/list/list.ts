import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { DetailPage } from '../detail/detail';
import { DetailUstadz } from '../detailUstadz/detailUstadz';
import { ListUstadz } from '../listUstadz/listUstadz';
import { NativeStorage } from '@ionic-native/native-storage';
import { LoadingController, Events } from 'ionic-angular';

import { OneSignal } from '@ionic-native/onesignal';

import 'rxjs/add/operator/map';
// import { map } from 'rxjs/operators';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  menuList: string = "listNgaji";
  selectedItem: any;
  icons: string[];
  notif: string[];
  jadwal: string[];
  ustadz: string[];
  banners: string[];
  items: Array<{title: string, note: string, icon: string}>;
//
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController, private nativeStorage: NativeStorage, private oneSignal: OneSignal, public events: Events)
  {
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');

    // this.events.subscribe('user1', (data) => {
    //   console.log('dari data');
    //   console.log(data);
    // })
    // this.events.publish('user12', 'hello');
    this.events.publish("user.logout");


    this.loadingCtrl.create({
      content: 'Please wait...',
      duration: 2000,
      dismissOnPageChange: true
    }).present();

    this.jadwal = [];
    let url = "http://www.waktungaji.com/apis/v2/week";
    this.http.get(url).map(res => res.json()).subscribe(data => {
      this.jadwal = data.list_jadwal;
    });

    this.banners = [];
    // let urlBanner = "banner";
    let urlBanner = "http://www.waktungaji.com/apis/v2/banner";
    this.http.get(urlBanner).map(res => res.json()).subscribe(data => {
      this.banners = data.list_jadwal;
      console.log(data.list_jadwal);
    });

    this.ustadz = [];
    let urlUstadz = "http://www.waktungaji.com/apis/v2/";
    this.http.get(urlUstadz).map(res => res.json()).subscribe(data => {
      this.ustadz = data.list_ustadz;
      console.log(data.list_ustadz);
    });


    // this.oneSignal.startInit('85fe1058-ccec-4db7-b21c-d9a6bc76ddb2', '532526724361');
    //
    // this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
    // this.notif = [];
    // this.oneSignal.handleNotificationReceived().subscribe((data:any) => {
    //  // do something when notification is received
    //  // this.notif = data;
    //  // console.log('data notif' + data.notification.payload.additionalData.uid);
    // });
    //
    // this.oneSignal.handleNotificationOpened().subscribe((notificationOpenedCallback) => {
    //         let notificationData = notificationOpenedCallback.notification.payload.additionalData;
    //         let  pushData = notificationData;
    //         console.log(pushData);
    //         if(pushData.Action == "detailJadwal") {
    //           console.log(pushData.Action);
    //           this.detailJadwal(pushData.id);
    //         }
    //     });
    //
    // this.nativeStorage.getItem('user')
    // .then(
    //   data => {
    //     this.oneSignal.sendTag("UserId", data.id_user);
    //   },
    //   error => {
    //     this.events.publish('user:logout');
    //
    //   }
    // );
    //
    //
    // this.oneSignal.endInit();

  }

  detail(detail) {
    this.navCtrl.push(DetailPage, {
      detail: detail
    });
  }

  detailJadwal(id) {
    this.navCtrl.push(DetailPage, {
      id: id
    });
  }

  detailUstadz(detail) {
    this.navCtrl.push(DetailUstadz, {
      detail: detail
    });
  }

  listUstadz() {
    this.navCtrl.push(ListUstadz, {});
  }

  loading(url) {
    this.loadingCtrl.create({
      content: 'Please wait open browser..',
      duration: 2000,
      dismissOnPageChange: true
    }).present();
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    this.jadwal = [];
    let url = "http://www.waktungaji.com/apis/month?p=2";
    this.http.get(url).map(res => res.json()).subscribe(data => {
      this.jadwal = data;
      console.log(this.jadwal);
    });

    this.ustadz = [];
    let urlUstadz = "http://www.waktungaji.com/apis/v2/";
    this.http.get(urlUstadz).map(res => res.json()).subscribe(data => {
      this.ustadz = data.list_ustadz;
      console.log(data.list_ustadz);
    });

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }
}
