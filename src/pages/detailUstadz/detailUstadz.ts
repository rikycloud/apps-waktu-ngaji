import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Events } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'detailUstadz.html'
})
export class DetailUstadz {

    detail:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events) {
      this.detail = navParams.get('detail');
      // this.id = 'testes';.

      console.log('log2..');
      this.events.publish('user1');
  }

}
